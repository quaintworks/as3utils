package com.quaintworks.utils {
	
	/**
	 * Date Utils
	 *  
	 * @author Quaint Works
	 * 
	 */
	public class DateUtils {
		/**
		 * Return current time 
		 * @return HH:MM:SS format
		 * 
		 */
		public static function getCurrentTime():String {
			return NumberUtils.leadingZero(new Date().getHours()) + ":" + NumberUtils.leadingZero(new Date().getMinutes()) + ":" + NumberUtils.leadingZero(new Date().getSeconds());
		}
		
		/**
		 * Convert seconds to a typical timer format 
		 * @param seconds
		 * @return HH:MM:SS format
		 * 
		 */
		public static function getFormattedTime(seconds:int):String {
			var hours:int = int(seconds / 3600);
			var mins:int = int((seconds - (hours * 3600)) / 60)
			var secs:int = seconds % 60;
			if (isNaN(hours) || isNaN(mins) || isNaN(secs)) {
				return "--:--:--";
			}
			
			return NumberUtils.leadingZero(hours) + ":" + NumberUtils.leadingZero(mins) + ":" + NumberUtils.leadingZero(secs);
		}
	}
}