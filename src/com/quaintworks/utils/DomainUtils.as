package com.quaintworks.utils {
	import flash.external.ExternalInterface;
	
	/**
	 * Domain Utils
	 *
	 * @author ADI
	 *
	 */
	public class DomainUtils {
		
		/**
		 * Retrieve the URL of the embedded SWF
		 * @return
		 *
		 */
		public static function getCurrentURL():String {
			var url:String = ExternalInterface.call("window.location.href.toString");
			return url;
		}
		
		/**
		 * Retrieve the domain of the embedded SWF
		 * @return
		 *
		 */
		public static function getCurrentDomain():String {
			var url:String = ExternalInterface.call("window.location.hostname.toString");
			return url;
		}
	}
}