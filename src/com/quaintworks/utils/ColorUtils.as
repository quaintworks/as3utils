package com.quaintworks.utils {
	
	/**
	 * Color Utils
	 *  
	 * @author Quaint Works
	 * 
	 */
	public class ColorUtils {
		private var red:Number;
		private var green:Number;
		private var blue:Number;
		
		public function ColorUtils(red:uint = 0, green:uint = 0, blue:uint = 0) {
			this.red = red;
			this.green = green;
			this.blue = blue;
		}
		
		/**
		 * Create ColorUtils color object from HEX value. Function creates object as a return variable.
		 *  
		 * @param hex
		 * @return 
		 * 
		 */
		public static function fromHEX(hex:Number):ColorUtils {
			var result:ColorUtils = new ColorUtils((hex & 0xFF0000) >> 16, (hex & 0x00FF00) >> 8, hex & 0x0000FF);
			return result;
		}
		
		/**
		 * Return HEX value of an color object 
		 * 
		 */
		public function getHEX():Number {
			return (red << 16 | green << 8 | blue);
		}
		
		public function getRed():Number {
			return red;
		}
		
		public function getGreen():Number {
			return green;
		}
		
		public function getBlue():Number {
			return blue;
		}
	}
}