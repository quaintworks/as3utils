package com.quaintworks.utils {
	
	/**
	 * Number Utils
	 *
	 * @author Quaint Works
	 *
	 */
	public class NumberUtils {
		/**
		 * Add leading zero if number is smaller than 10
		 * @param number
		 * @return
		 *
		 */
		public static function leadingZero(number:int):String {
			if (number < 10)
				return "0" + number;
			else
				return number.toString();
		}
		
		/**
		 * Calculate the difference between two numbers
		 * @param number1
		 * @param number2
		 * @return
		 *
		 */
		public static function getNumberDifference(number1:Number, number2:Number):Number {
			var diff:Number = number1 - number2;
			if (diff < 0) {
				diff *= -1;
			}
			return diff;
		}
	}
}