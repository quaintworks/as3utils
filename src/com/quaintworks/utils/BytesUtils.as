package com.quaintworks.utils {

	public class BytesUtils {
		private static var KB:uint=1024;
		private static var MB:uint=1024 * KB;
		private static var GB:uint=1024 * MB;

		public static function getMB(bytes:Number):String {
			return Number(bytes / MB).toFixed(2) + " MB";
		}

		public static function getKB(bytes:Number):String {
			return Number(bytes / KB).toFixed(2) + " KB";
		}

		public static function getB(bytes:Number):String {
			return bytes + "B";
		}

		public static function getAuto(bytes:Number):String {
			if (bytes > MB)
				return Number(bytes / MB).toFixed(2) + " MB";
			else if (bytes > KB)
				return Number(bytes / KB).toFixed(2) + " KB";
			else
				return bytes + " B";
		}
	}

}
