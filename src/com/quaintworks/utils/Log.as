package com.quaintworks.utils {
	import flash.external.ExternalInterface;
	
	/**
	 * Log traces into IDE output and web browser's console log window
	 * @author Quaint Works
	 *
	 */
	public class Log {
		// Log to Javascript console (also traces in IDE).
		/**
		 * If true, log traces into web browser's console log window
		 */
		static public var doLog:Boolean = true;
		
		/**
		 * If true, log traces into IDE output window
		 */
		static public var doTrace:Boolean = true;
		
		/**
		 * @param rest List of parameters to trace
		 * 
		 */
		public static function add(... rest):void {
			var s:String = "Log >> " + rest.join(" ");
			
			if (doLog && ExternalInterface.available)
				ExternalInterface.call("console.log", s);
			if (doTrace)
				trace(s);
		}
	}
}