package com.quaintworks.utils {
	
	/**
	 * String Utils
	 * 
	 * @author Quaint Works
	 * 
	 */
	public class StringUtils {
		/**
		 * Generate random string 
		 * @param newLength
		 * @param userAlphabet
		 * @return 
		 * 
		 */
		public static function generateRandomString(newLength:uint = 1, userAlphabet:String = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"):String {
			var alphabet:Array = userAlphabet.split("");
			var alphabetLength:int = alphabet.length;
			var randomLetters:String = "";
			
			for (var i:uint = 0; i < newLength; i++) {
				randomLetters += alphabet[int(Math.floor(Math.random() * alphabetLength))];
			}
			return randomLetters;
		}
		
		/**
		 * Validate an email address 
		 * @param str Email address
		 * @return 
		 * 
		 */		
		public static function validateEmail(str:String):Boolean {
			var pattern:RegExp = /(\w|[_.\-])+@((\w|-)+\.)+\w{2,4}+/;
			var result:Object = pattern.exec(str);
			if (result == null) {
				return false;
			}
			return true;
		}
		
		/**
		 * Trim a string using regexp 
		 * @param p_string
		 * @return 
		 * 
		 */
		public static function trim(p_string:String):String {
			if (p_string == null) {
				return '';
			}
			return p_string.replace(/^\s+|\s+$/g, '');
		}
	}
}