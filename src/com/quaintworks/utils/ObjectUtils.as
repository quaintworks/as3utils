package com.quaintworks.utils {
	import flash.system.ApplicationDomain;
	import flash.utils.getQualifiedClassName;
	
	/**
	 * Object Utils 
	 * @author Quaint Works
	 * 
	 */
	public class ObjectUtils {
		/**
		 * Count all properties in a given object 
		 * @param obj
		 * @return Properties number
		 * 
		 */
		public static function numProperties(obj:Object):int {
			var count:int = 0;
			
			for each (var prop:Object in obj) {
				count++;
			}
			return count;
		}
		
		/**
		 * Checks for a property in given Object 
		 * @param obj
		 * @return Boolean
		 * 
		 */
		public static function hasProperties(obj:Object):Boolean {
			for each (var prop:Object in obj) {
				return true;
			}
			return false;
		}
		
		/**
		 * Copy given object into new object while copying all its properties 
		 * @param obj
		 * @param into
		 * @return 
		 * 
		 */
		public static function copy(obj:Object, into:Object = null):Object {
			if (into == null) {
				into = {};
			}
			
			if (obj != null) {
				for (var o:* in obj) {
					into[o] = obj[o];
				}
			}
			return into;
		}
		
		/**
		 * Convert object into one level Array 
		 * @param obj
		 * @return 
		 * 
		 */
		public static function toArray(obj:Object):Array {
			if (obj == null) {
				return null;
			} else {
				var ret:Array = [];
				
				for each (var prop:Object in obj) {
					ret.push(prop);
				}
				return ret;
			}
		}
		
		/**
		 * Parse object into XML format 
		 * @param p_obj
		 * @param p_nodeName
		 * @return 
		 * 
		 */
		public static function parseObject(p_obj:Object, p_nodeName:String = "root"):String {
			var str:String = "<" + p_nodeName + ">";
			for (var items:String in p_obj) {
				if (typeof(p_obj[items]) == "object") {
					str += parseObject(p_obj[items], items);
				} else {
					var nodeValue:* = p_obj[items];
					str += "<" + items + ">" + nodeValue + "</" + items + ">";
				}
			}
			str += "</" + p_nodeName + ">";
			return str;
		}
		
		/**
		 * Get the class of an object 
		 * @param obj
		 * @return 
		 * 
		 */		
		public static function getClass(obj:Object):Class {
			return ApplicationDomain.currentDomain.getDefinition(getQualifiedClassName(obj)) as Class;
		}
	
	}
}