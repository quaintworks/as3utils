package com.quaintworks.utils {
	
	/**
	 * Array Utils 
	 * 
	 * @author Quaint Works
	 * 
	 */
	public class ArrayUtils {
		
		/**
		 * Search for a number in array and return closest value.
		 * 
		 * @param array
		 * @param number
		 * @return Closest value to a given number
		 * 
		 */
		public static function getSnapValue(array:Array, number:Number):Number {
			var closestNum:Number = new Number;
			var closestArrayItem:Number = new Number;
			var tempNum:Number = new Number;
			var arrayLength:int = array.length;
			
			for (var i:int = 0; i < arrayLength; i++) {
				tempNum = Math.abs(array[i] - number);
				
				if (tempNum < closestNum || i == 0) {
					closestNum = tempNum;
					closestArrayItem = i;
				}
			}
			
			return array[closestArrayItem];
		}
	}
}